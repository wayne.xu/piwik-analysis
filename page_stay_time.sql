-- set @prod = 'Betway CN';
-- set @dtm_from = str_to_date('2020-08-31 00:00:00', '%Y-%m-%d %H:%i:%s');
-- set @dtm_to = str_to_date('2020-08-31 23:59:59', '%Y-%m-%d %H:%i:%s');
--  P1M1 App
set @prod = 'P1M1 SPORTSAPP'; 
set @dtm_from = str_to_date('2021-03-03 00:00:00', '%Y-%m-%d %H:%i:%s') - interval 8 hour;
set @dtm_to = str_to_date('2021-03-24 23:59:59', '%Y-%m-%d %H:%i:%s') - interval 8 hour;
set @idsite = (
		select idsite
		from analytics.site
		where name = @prod
	)
;

-- Calculate average staying time of event_category.

select
	processed_subset.product,
    processed_subset.event_category,
	date_format(log_time + interval 8 hour, '%Y-%m-%d') as 'date',
	count(processed_subset.event_category) as 'count',
	min(
		replace(substring(processed_subset.stay_time, 1, 3), '"', '') * 10 + 
    	replace(substring(processed_subset.stay_time, 5, 6), '"', '')
	) as 'minimum_stay_time',
	max(
		replace(substring(processed_subset.stay_time, 1, 3), '"', '') * 10 + 
    	replace(substring(processed_subset.stay_time, 5, 6), '"', '')
	) as 'maximum_stay_time',
    round(
    	avg(
    		replace(substring(processed_subset.stay_time, 1, 3), '"', '') * 10 + 
    		replace(substring(processed_subset.stay_time, 5, 6), '"', '')
    	), 2
    ) as 'avg_stay_time'
from (
	select 
		subset.product,
		subset.user_id,
		subset.session_id,
		subset.session_id_va,
		subset.event_category,
		subset.event_action,
		-- subset.event_name,
	    json_extract(
	    		replace(replace(replace(event_name, '_  &quot;', '"'), '&quot;_', '"'), '&quot;', '"'), "$.StayTime"
	    	) as "stay_time",
	    subset.event_url,
	    subset.log_time,
	    subset.visit_first_action_time,
	    subset.visit_last_action_time
	from (
		select
			@prod as product,
		    lv.user_id,
		    llva.idvisit as session_id, /*count(Conversion-a tally that counts) of desired action taken by visitor: visit action query: session id*/
		    llva.idlink_va as session_id_va, /*Id of specific visit action that resulted in conversion*/
		    la_ev_cat.name as event_category,/*action type*/
			la_ev_act.name as event_action,
		    la_name.name as event_name,
		    la_url.name as event_url,
		    llva.server_time as log_time,/*Datetime of conversion in UTC timezone*/
			lv.visit_first_action_time as visit_first_action_time,/*Datetime of visit's first action*/
		    lv.visit_last_action_time as visit_last_action_time /*Datetime of visit's last action*/
		from analytics.log_link_visit_action llva /*This table contains list of all actions - one for each action visitor makes during the visit*/
		inner join analytics.log_visit lv /*log visit table - inner join to get intersection of idvisit*/ 
			on llva.idvisit = lv.idvisit
		left join analytics.log_action la_ev_act /*This table contains action types(specific url,page title, visits), which pages are more relevant to visitors*/
			on llva.idaction_event_action = la_ev_act.idaction
		left join analytics.log_action la_ev_cat
			on llva.idaction_event_category = la_ev_cat.idaction
		left join analytics.log_action la_name
			on llva.idaction_name = la_name.idaction
		left join analytics.log_action la_url
			on llva.idaction_url = la_url.idaction
		where 1=1
			and llva.idsite = @idsite 
			and llva.server_time between @dtm_from and @dtm_to
			and lv.idsite = @idsite
			and lv.visit_last_action_time between @dtm_from and @dtm_to
			and lv.visit_first_action_time between @dtm_from and @dtm_to
		order by lv.user_id, llva.idvisit, llva.idlink_va
	) as subset
	where 1 = 1 and 
		subset.event_category in (
			'BW_FMBetRecommend_View_Page', 'BW_FMStreamingText_View_Page', 'BW_FMCalender_View_Page', 'BW_FMPreStats_View_Page',
			'BW_FMAftStats_View_Page', 'BW_Message_View_Page', 'BW_TOChecking_View_Page'
		)
) as processed_subset
group by event_category, date_format(log_time + interval 8 hour, '%Y-%m-%d');


--------------------------------------------------------------------------------
--- for 'P1M1 App'

select
	session_actions.session_id as 'session_id',  
	group_concat(session_actions.event_category) as 'group_categories',
	group_concat(session_actions.session_id_va) as 'group_action_ids',
	count(session_actions.event_category) as 'category_count',
	min(unix_timestamp(session_actions.visit_last_action_time)) - 
	min(unix_timestamp(session_actions.visit_first_action_time)) as 'cost_time'
from (
	select
		@prod as product,
		lv.user_id,
		llva.idvisit as session_id, /*count(Conversion-a tally that counts) of desired action taken by visitor: visit action query: session id*/
		llva.idlink_va as session_id_va, /*Id of specific visit action that resulted in conversion*/
		la_ev_cat.name as event_category,/*action type*/
		lv.visit_first_action_time as visit_first_action_time,/*Datetime of visit's first action*/
		lv.visit_last_action_time as visit_last_action_time /*Datetime of visit's last action*/
	from analytics.log_link_visit_action llva /*This table contains list of all actions - one for each action visitor makes during the visit*/
	inner join analytics.log_visit lv /*log visit table - inner join to get intersection of idvisit*/ 
		on llva.idvisit = lv.idvisit
	left join analytics.log_action la_ev_cat
		on llva.idaction_event_category = la_ev_cat.idaction
	left join analytics.log_action la_name
		on llva.idaction_name = la_name.idaction
	where 1=1
		and llva.idsite = @idsite 
		and llva.server_time between @dtm_from and @dtm_to
		and lv.idsite = @idsite
		and lv.visit_last_action_time between @dtm_from and @dtm_to
		and lv.visit_first_action_time between @dtm_from and @dtm_to
	order by lv.user_id, llva.idvisit, llva.idlink_va
) session_actions
group by session_actions.session_id
having group_categories like '%BW_FMMain_Click_BetRecomment%'
		and (
			group_categories like '%sportbook%' or group_categories like '%imsb%'
		);


select *
from (
	select
		session_actions.session_id as 'session_id',  
		group_concat(session_actions.event_category) as 'group_categories',
		count(session_actions.event_category) as 'category_count',
		min(unix_timestamp(session_actions.visit_last_action_time)) - 
		min(unix_timestamp(session_actions.visit_first_action_time)) as 'cost_time'
	from (
		select
			@prod as product,
			lv.user_id,
			llva.idvisit as session_id, /*count(Conversion-a tally that counts) of desired action taken by visitor: visit action query: session id*/
			la_ev_cat.name as event_category,/*action type*/
			llva.server_time as log_time,/*Datetime of conversion in UTC timezone*/
			lv.visit_first_action_time as visit_first_action_time,/*Datetime of visit's first action*/
			lv.visit_last_action_time as visit_last_action_time /*Datetime of visit's last action*/
		from analytics.log_link_visit_action llva /*This table contains list of all actions - one for each action visitor makes during the visit*/
		inner join analytics.log_visit lv /*log visit table - inner join to get intersection of idvisit*/ 
			on llva.idvisit = lv.idvisit
		left join analytics.log_action la_ev_cat
			on llva.idaction_event_category = la_ev_cat.idaction
		left join analytics.log_action la_name
			on llva.idaction_name = la_name.idaction
		where 1=1
			and llva.idsite = @idsite 
			and llva.server_time between @dtm_from and @dtm_to
			and lv.idsite = @idsite
			and lv.visit_last_action_time between @dtm_from and @dtm_to
			and lv.visit_first_action_time between @dtm_from and @dtm_to
	) session_actions
	group by session_actions.session_id
	having group_categories like '%BW_FMMain_Click_BetRecomment%'
			and (
				group_categories like '%sportbook%' or group_categories like '%imsb%'
			)
) subset
where subset.group_categories regexp 'BW_FMBetRecommend_ManagerRecommend_Click_Match.*sportbook' 
	or subset.group_categories regexp 'BW_FMBetRecommend_ManagerRecommend_Click_Match.*imsb';

-------------------

with subset as (
	select
		@prod as product,
		lv.user_id,
		llva.idvisit as session_id, /*count(Conversion-a tally that counts) of desired action taken by visitor: visit action query: session id*/
		la_ev_cat.name as event_category,/*action type*/
		llva.server_time as log_time,/*Datetime of conversion in UTC timezone*/
		lv.visit_first_action_time as visit_first_action_time,/*Datetime of visit's first action*/
		lv.visit_last_action_time as visit_last_action_time /*Datetime of visit's last action*/
	from analytics.log_link_visit_action llva /*This table contains list of all actions - one for each action visitor makes during the visit*/
	inner join analytics.log_visit lv /*log visit table - inner join to get intersection of idvisit*/ 
		on llva.idvisit = lv.idvisit
	left join analytics.log_action la_ev_cat
		on llva.idaction_event_category = la_ev_cat.idaction
	left join analytics.log_action la_name
		on llva.idaction_name = la_name.idaction
	where 1=1
		and llva.idsite = @idsite 
		and llva.server_time between @dtm_from and @dtm_to
		and lv.idsite = @idsite
		and lv.visit_last_action_time between @dtm_from and @dtm_to
		and lv.visit_first_action_time between @dtm_from and @dtm_to
),
exists_event_sessions as (
	-- Get session which include event BW_FMMain_Click_BetRecomment and sportbook or imsb.
	select t1.session_id
	from (
		select distinct session_id
		from subset
		where event_category = 'BW_FMMain_Click_BetRecomment'
	) t1
	inner join (
		select distinct session_id
		from subset
		where event_category = 'sportbook' or event_category = 'imsb'
	) t2
	on t1.session_id = t2.session_id
),
filter_session_table as (
	select subset.* 
	from subset 
	inner join exists_event_sessions
	on subset.session_id = exists_event_sessions.session_id
),
group_event_category_table as (
	select
		session_id,
		group_concat(event_category) as 'group_categories',
		count(event_category) as 'count_category'
	from filter_session_table
	group by session_id
)

select * from group_event_category_table
where 1=1
	and group_categories regexp 'BW_FMBetRecommend_ManagerRecommend_Click_Match.*sportbook'
	or group_categories regexp 'BW_FMBetRecommend_ManagerRecommend_Click_Match.*imsb' 