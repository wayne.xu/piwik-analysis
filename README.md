## Requirement

- [ticket](https://hermes.devgambit.net/browse/BDE-23?jql=project%20%3D%20BDE)

## Project Structure

```ascii
├── reports/
├── config/
├── page_stay_time.sql
├── survival_rate.sql
├── betway_tracking_code.py
└── utils.py
├── sql_templates.py
```

- `report`: The directory saves all of the reports.
- `config`: The directory contains the configuration of the project.
- `page_stay_time.sql`: The SQL file calculate the stay time of the specific page.
- `survival_rate.sql`: The SQL file calculate the the nunber of count and the click rate of specific events.
- `betway_tracking_code.py`: The script is responsible for generating the reports which are the ticket need.
- `utils.py`: It offers the utility function which be used by other scripts.
- `sql_templates.py`: It offers the SQL statement template which be used by other scripts.

## How to execute the project?

1. Fill the database information on `./config/config.cfg`

2. Execute the `betway_tracking_code.py`

   ```bash
   python betway_tracking_code.py
   ```
3. The reports will save on `./reports` directory.

   
