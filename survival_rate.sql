set @prod = 'P1M1 App'; 
set @dtm_from = str_to_date('2021-03-24 00:00:00', '%Y-%m-%d %H:%i:%s') - interval 8 hour;
set @dtm_to = str_to_date('2021-03-24 23:59:59', '%Y-%m-%d %H:%i:%s') - interval 8 hour;
set @idsite = (
		select idsite
		from analytics.site
		where name = @prod
	)
;
-- BW_FMMain_Click_BetRecomment -> .... -> BW_FMBetRecommend_ManagerRecommend_Click_Match -> sportbook / imsb
-- Below SQL statement is to calculate count and click rate of specific event.

with filter_table as (
    select
        subset.product as product,
        subset.session_id as session_id,
        subset.session_id_va as session_id_va,
        subset.event_category as event_category,
        subset.event_action as event_action,
        subset.event_name as event_name,
        subset.log_time as log_time,
        row_number() over (partition by session_id order by log_time, session_id_va) as 'step'
    from (
        select
            @prod as product,
            llva.idvisit as session_id, 
            llva.idlink_va as session_id_va, 
            la_ev_cat.name as event_category,
            la_ev_act.name as event_action,
            la_name.name as event_name,
            llva.server_time as log_time
        from analytics.log_link_visit_action llva 
        inner join analytics.log_visit lv
            on llva.idvisit = lv.idvisit
        left join analytics.log_action la_ev_act
            on llva.idaction_event_action = la_ev_act.idaction
        left join analytics.log_action la_ev_cat
            on llva.idaction_event_category = la_ev_cat.idaction
        left join analytics.log_action la_name
            on llva.idaction_name = la_name.idaction
        where 1=1
            and llva.idsite = @idsite 
            and llva.server_time between @dtm_from and @dtm_to
            and lv.idsite = @idsite
            and lv.visit_last_action_time between @dtm_from and @dtm_to
            and lv.visit_first_action_time between @dtm_from and @dtm_to
            and la_ev_cat.name is not null
    ) as subset
    inner join (
        select
            distinct llva.idvisit as session_id
        from analytics.log_link_visit_action llva 
        inner join analytics.log_visit lv
            on llva.idvisit = lv.idvisit
        where 1=1
            and llva.idsite = @idsite 
            and llva.server_time between @dtm_from and @dtm_to
            and lv.idsite = @idsite
            and lv.visit_last_action_time between @dtm_from and @dtm_to
            and lv.visit_first_action_time between @dtm_from and @dtm_to
            and llva.idaction_event_category = 115289663
            -- The id of BW_FMMain_Click_BetRecomment event_categroy from log_action table.
    ) as target_sessions
    on subset.session_id = target_sessions.session_id
),
step_table as (
    select
        t1.product as 'product',
        t1.session_id as 'session_id',
        t1.event_category as 't1_event_category',
        t2.event_category as 't2_event_category',
        t1.session_id_va as 't1_session_id_va',
        t2.session_id_va as 't2_session_id_va',
        t1.event_name as 't1_event_name',
        t2.event_name as 't2_event_name',
        t1.step as 't1_step',
        t2.step as 't2_step',
        t1.log_time as 'log_time'
    from filter_table t1
    left join filter_table t2
    on t1.session_id = t2.session_id 
        and t1.step = t2.step -1
)

select
    log_time,
    sum(trans_BW_FMMain_Click_BetRecomment) as 'Click_BetRecomment_count',
    sum(trans_BW_FMBetRecommend_ManagerRecommend_Click_Match) as 'ManagerRecommend_Click_Match_count',
    sum(trans_complete_flow) as 'complete_event_count',
    case when sum(trans_BW_FMMain_Click_BetRecomment) = 0 then 0.0
        else round(sum(trans_complete_flow)/sum(trans_BW_FMMain_Click_BetRecomment), 2)
    end as 'Click_BetRecomment_click_rate',
    case when sum(trans_BW_FMBetRecommend_ManagerRecommend_Click_Match) = 0 then 0.0
        else round(sum(trans_complete_flow)/sum(trans_BW_FMBetRecommend_ManagerRecommend_Click_Match), 2)
    end as 'ManagerRecommend_Click_Match_click_rate'
from (
    select
        date_format(log_time + interval 8 hour, '%Y-%m-%d') as 'log_time',  
        case
            when t1_event_category = 'BW_FMMain_Click_BetRecomment'
            then 1 else 0
        end as 'trans_BW_FMMain_Click_BetRecomment',
        case
            when t1_event_category = 'BW_FMBetRecommend_ManagerRecommend_Click_Match'
            then 1 else 0
        end as 'trans_BW_FMBetRecommend_ManagerRecommend_Click_Match',
        case when t1_event_category = 'BW_FMBetRecommend_ManagerRecommend_Click_Match'
                and t2_event_category in ('sportbook', 'imsb') 
            then 1 else 0
        end as 'trans_complete_flow'
    from step_table
) trans_table
group by log_time;

