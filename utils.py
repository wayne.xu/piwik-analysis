import re, json, time

from configparser import ConfigParser

from functools import wraps
from contextlib import contextmanager
from typing import Any, Union, Optional, Callable

from pymysql import connect, cursors
from pymysql.constants.CLIENT import MULTI_STATEMENTS

class Configuration():
    
    FILE_PATH = './config/config.cfg'
    BRACKET_PAT = re.compile(r'[\[|\{]')
    ENCODING = 'utf-8'
    
    @classmethod
    def get_value(cls, section, option, file_path=None, **kwargs) -> Any:
        config = ConfigParser()
        try:
            if file_path:
                config.read(file_path, encoding=cls.ENCODING)
            else:
                config.read(cls.FILE_PATH, encoding=cls.ENCODING)

            value = config.get(section, option, **kwargs)
        except Exception as e:
            raise e

        if cls.BRACKET_PAT.match(value):
            value = json.load(value)

        try:
            value = int(value)
        except ValueError:
            pass
        
        return value


class MySQLConn():
    
    def __init__(
            self,
            host: str,
            port: Union[int, str],
            user: str,
            password:str,
            database: Optional[str]=None,
            **kwargs
        ):
        self.args = {
            'host': host,
            'port': port,
            'user': user,
            'password': password
        }
        if database:
            self.args['database'] = database
        self.args.setdefault("database", None)
        self.args.setdefault("cursorclass", cursors.DictCursor)
        self.args.update(kwargs)
        self.conn = connect(**self.args)

    @contextmanager
    def auto_commit(self):
        try:
            cur = self.conn.cursor()
            yield cur
            self.conn.commit()
        except Exception as e:
            self.conn.rollback()
            raise e
        finally:
            cur.close()

    @contextmanager
    def auto_cursor(self):
        try:
            cur = self.conn.cursor()
            yield cur
        except Exception as e:
            raise e
        finally:
            cur.close()

    def query_all(self, sql_stmt: str, values: tuple=()) -> tuple:
        with self.auto_cursor() as cursor:
            if values:
                cursor.execute(sql_stmt, values)
            else:
                cursor.execute(sql_stmt)
            row_count = cursor.rowcount
            result = cursor.fetchall()
        return row_count, result

    def query_one(self, sql_stmt: str, values: tuple=()) -> tuple:
        with self.auto_cursor() as cursor:
            if values:
                cursor.execute(sql_stmt, values)
            else:
                cursor.execute(sql_stmt)
            result = cursor.fetchone()
        return result


def timer(func: Callable) -> Callable:
    '''
    Calculate the execution time of the decorated function.
    '''
    @wraps(func)
    def wrapper_func(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        elapsed_time = end_time - start_time
        print("{} elapsed time: {:.2f}".format(func.__name__, elapsed_time))
        return result

    return wrapper_func