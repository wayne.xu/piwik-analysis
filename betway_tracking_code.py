import os, re, json

import pandas as pd

from typing import Optional, Generator
from datetime import datetime, timedelta

from pymysql.constants.CLIENT import MULTI_STATEMENTS

from sql_templates import target_event_template
from utils import Configuration, MySQLConn, timer

class BetwayTrackingCodeAnalysis():
    
    def __init__(self, host, port, user, password, database=None, **kwargs):
        self.db_info = {
            'host': host,
            'port': port,
            'user': user,
            'password': password,
            'database': database,
        }
        self.db_info.setdefault('client_flag', MULTI_STATEMENTS)
        self.db_info.update(kwargs)
        self.db = MySQLConn(**self.db_info)

    def get_date_range(
            self,
            num_days: int,
            base_date: Optional[datetime]=None,
            date_format: str="%Y-%m-%d %H:%M:%S"
        ) -> Generator:
        '''
        Get all date string in specific date range.
        '''
        if base_date is None:
            base_date = datetime.now()
        if date_format is None:
            date_list = ((base_date - timedelta(days=d)) for d in range(num_days))
        else:
            date_list = ((base_date - timedelta(days=d)).strftime(date_format) for d in range(num_days))
        return date_list

    def count_flow(
            self,
            user_flows: list,
            start_event_category: str='BW_FMMain_Click_BetRecomment',
            end_categories: tuple=('sportbook', 'imsb')
        ) -> dict:
        '''
        Find the user flow whether confirm the specific condition.
        If user flow contains start_event_category before end_categroy in the same session_id,
        it will count.
        '''
        category_pattern = re.compile(r'(BetRecommen[dt])|(sportbook)|(imsb)|(main)')
        
        current_session_id = 0
        complete_flow = False
        count_result = dict()
        flow_container = list()
        event_args_container = list()
        for flow_info in user_flows:
            session_id = flow_info['session_id']
            event_category = flow_info['event_category']
            event_name = flow_info['event_name']

            # Initialize data and control flow variables
            # 1. The session_id change.
            # 2. The event_category doesn't conform the pattern. 
            if current_session_id != session_id or\
                category_pattern.search(event_category) is None:

                current_session_id = session_id
                if session_id not in count_result:
                    count_result[session_id] = []

                flow_container, event_args_container = [], []
                record_flow = False
                complete_flow = False
                
            if event_category == start_event_category:
                # Start record
                record_flow = True

            if record_flow:
                # This block control how to append data.
                flow_container.append(event_category)
                event_args_container.append(event_name)
                if event_category in end_categories:
                    complete_flow = True

            if complete_flow:
                # Find the target flow and finish to count flow.
                count_result[session_id].append({'user_flow': flow_container, 'event_args': event_args_container})
                # Reset flow_container and event_args_container
                flow_container, event_args_container = [], []
                record_flow = False
                complete_flow = False
        
        # Remove empty data
        count_result = {session_id: data for session_id, data in count_result.items() if data}

        return count_result
    
    @timer
    def fliter_by_date(self, num_days: int, base_date: Optional[datetime]):        
        default_kwargs = {
            'product_name': 'P1M1 App',
            'id_site': 211,
        }
        # Get all date rage
        all_dates = self.get_date_range(num_days, base_date, date_format=None)
        all_date_times = dict()
        for d in all_dates:
            date_key = d.strftime('%Y-%m-%d')
            start_time = (d - timedelta(hours=8)).strftime('%Y-%m-%d %H:%M:%S')
            end_time = (d + timedelta(hours=15, minutes=59)).strftime('%Y-%m-%d %H:%M:%S')
            all_date_times[date_key] = {'start_time': start_time, 'end_time': end_time}

        self.filter_by_date_result = dict() # Record the result by different date.
        for date_key, date_info in all_date_times.items():
            default_kwargs.update(date_info)
            # Fill the variable to SQL template.
            sql_stmt = target_event_template.format(**default_kwargs)
            _, result = self.db.query_all(sql_stmt)
            result = self.count_flow(result)
            self.filter_by_date_result[date_key] = result
            print('Successfully filter {} tracking data'.format(date_key))

    def clean_event_args(self, event_args: str) -> dict:
        replace_pat = re.compile(r'(_\s*)?(&quot;\s?)(_)?')
        event_args = replace_pat.sub('"', event_args)
        event_args = json.loads(event_args, encoding='utf-8')
        return event_args

    def trans_stay_time_to_sec(self, stay_time: str) -> int:
        minute, secs = stay_time.split(':')
        total_secs = int(minute) * 60 + int(secs)
        return total_secs

    def calculate_stay_time(self, user_flow: list, event_args: list) -> dict:
        '''
        Calculate the number of count and stay_time of the series_event.
        '''
        series_event = 'BW_FMBetRecommend_View_Page'
        series_event_count = 0
        total_stay_time = 0
        for event_category, args in zip(user_flow, event_args):
            if series_event_count > 0 and event_category != series_event:
                break
            elif series_event_count > 0 and event_category == series_event:
                args = self.clean_event_args(args)
                try:
                    stay_time = args['StayTime']
                    total_stay_time += self.trans_stay_time_to_sec(stay_time)
                except KeyError:
                    print('-'*100)
                    print(f"The args can't retrieve stay time: {args}")
                    raise

            if event_category == series_event:
                series_event_count += 1

        concate_event_categroy = ''
        if total_stay_time > 0:
            concate_event_categroy = ','.join(user_flow)

        return {
            'seriers_event_count': series_event_count,
            'total_stay_time': total_stay_time,
            'user_flow': concate_event_categroy,
        }

    def get_daily_report(self):
        try:
            self.filter_by_date_result
        except AttributeError:
            raise AttributeError((
                "Need to execute function fliter_by_date to get "
                "the variable filter_by_date_result"
            ))

        self.daily_count_report = list() # For daily report
        self.daily_flow = list()
        self.daily_view_flow = list()
        for date_key, daily_result in self.filter_by_date_result.items():
            daily_count, daily_view_view_count = 0, 0
            for session_id, results in daily_result.items():
                daily_count += len(results)
                for result in results:
                    calculate_result = self.calculate_stay_time(result['user_flow'], result['event_args'])
                    if calculate_result['total_stay_time'] != 0:                        
                        daily_view_view_count += 1
                        concate_flow = calculate_result['user_flow']
                        flow_report = {
                            'date': date_key,
                            'view_view_flow': concate_flow,
                            'stay_time': calculate_result['total_stay_time'],
                        }
                        self.daily_view_flow.append(flow_report)
                    else:
                        concate_flow = ','.join(result['user_flow'])
                        flow_report = {
                            'date': date_key,
                            'flow': concate_flow,
                            'stay_time': 0,
                        }
                        self.daily_flow.append(flow_report)

            daily_report = {'date': date_key, 'daily_count': daily_count, 'daily_view_count': daily_view_view_count}
            self.daily_count_report.append(daily_report)
            print(f'date: {date_key}, daily_count: {daily_count}, daily_view_view_count: {daily_view_view_count}')

    def export_report(self, output_path: str, report_type: str):
        all_report_info = {
            'daily_count': {
                'dataset': self.daily_count_report,
                'columns': ['date', 'daily_count', 'daily_view_view_count'],
            },
            'daily_flow': {
                'dataset': self.daily_flow,
                'columns': ['date', 'flow', 'stay_time'],
            },
            'daily_view_flow': {
                'dataset': self.daily_view_flow,
                'columns': ['date', 'view_view_flow', 'stay_time'],
            }
        }
        report_info = all_report_info[report_type]
        df = pd.DataFrame(self.daily_count_report, columns=report_info['columns'])
        df.to_csv(output_path, index=False, encoding='utf-8')
        print('Output {} to {}'.format(report_type, output_path))

if __name__ == '__main__':
    reports_folder = './reports'
    betway_analysis = BetwayTrackingCodeAnalysis(
        host=Configuration.get_value('database', 'HOST'),
        port=Configuration.get_value('database', 'PORT'),
        database=Configuration.get_value('database', 'DATABASE'),
        user=Configuration.get_value('database', 'USER'),
        password=Configuration.get_value('database', 'PASSWORD'),
        client_flag=MULTI_STATEMENTS
    )
    betway_analysis.fliter_by_date(22, datetime(2021, 3, 24))
    betway_analysis.get_daily_report()
    # There are three types of report.
    # 1. daily_count
    # 2. daily_flow 
    # 3. daily_view_flow
    all_output_info = [
        {
            'report_type': 'daily_count',
            'output_path': os.path.join(reports_folder, 'daily_count_report.csv')
        },
        {
            'report_type': 'daily_flow',
            'output_path': os.path.join(reports_folder, 'daily_flow_repoty.csv')
        },
        {
            'report_type': 'daily_view_flow',
            'output_path': os.path.join(reports_folder, 'daily_view_flow_report.csv')
        },
    ]
    for output_info in all_output_info:
        betway_analysis.export_report(**output_info)