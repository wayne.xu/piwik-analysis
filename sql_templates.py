idsite_sql_template = """
    select `idsite` 
    from analytics.site 
    where `name` = '{product_name}'
"""

target_event_template = """
select
    subset.product as product,
    subset.session_id as session_id,
    subset.session_id_va as session_id_va,
    subset.event_category as event_category,
    subset.event_action as event_action,
    subset.event_name as event_name,
    subset.log_time as log_time
from (
    select
        '{product_name}' as product,
        llva.idvisit as session_id, 
        llva.idlink_va as session_id_va, 
        la_ev_cat.name as event_category,
        la_ev_act.name as event_action,
        la_name.name as event_name,
        llva.server_time as log_time
    from analytics.log_link_visit_action llva 
    inner join analytics.log_visit lv
        on llva.idvisit = lv.idvisit
    left join analytics.log_action la_ev_act
        on llva.idaction_event_action = la_ev_act.idaction
    left join analytics.log_action la_ev_cat
        on llva.idaction_event_category = la_ev_cat.idaction
    left join analytics.log_action la_name
        on llva.idaction_name = la_name.idaction
    where 1=1
        and llva.idsite = {id_site} 
        and llva.server_time between '{start_time}' and '{end_time}'
        and lv.idsite = {id_site}
        and lv.visit_last_action_time between '{start_time}' and '{end_time}'
        and lv.visit_first_action_time between '{start_time}' and '{end_time}'
        and la_ev_cat.name is not null
) as subset
inner join (
    select
        distinct llva.idvisit as session_id
    from analytics.log_link_visit_action llva 
    inner join analytics.log_visit lv
        on llva.idvisit = lv.idvisit
    where 1=1
        and llva.idsite = {id_site} 
        and llva.server_time between '{start_time}' and '{end_time}'
        and lv.idsite = {id_site}
        and lv.visit_last_action_time between '{start_time}' and '{end_time}'
        and lv.visit_first_action_time between '{start_time}' and '{end_time}'
        and llva.idaction_event_category = 115289663
) as target_sessions
on subset.session_id = target_sessions.session_id
order by subset.log_time, subset.session_id_va
"""
